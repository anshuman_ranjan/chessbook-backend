const express = require("express");
const User = require("../models/User");
const mongoose = require("mongoose");

exports.registerUser = async (req, res) => {
  try {
    const result = await new User(req.body).save();
    res.send(result);
  } catch (err) {
    console.log(err);
    res.send(err);
  }
};

exports.viewScoresheetSharedWithMe = async (req, res) => {
  const { UserId, ScoresheetId, accept } = req.body;
  // return console.log(
  //   `UserID:.....`,
  //   UserId,
  //   `ScoreSheetId:.......`,
  //   ScoresheetId
  // );
  if (UserId) {
    if (accept === "yes") {
      //Allowing the user to view the Scoresheet shared with him/her.
      res.sendStatus(200).redirect("/scoreSheet/viewScoresheet:ScoresheetId");
    } else res.send("You have now denied the request.");
  }
};

exports.shareScoresheet = async (req, res) => {
  const { userId, scoreSheetId, shareWith } = req.body;
  // return console.log(userId, scoreSheetId);
  res.json({
    message: "Scoresheet shared by: ",
    userId,
    anotherMessage: "and scoreSheet Id: ",
    scoreSheetId,
    sharedWith: "and shared with :",
    shareWith,
  });
};

exports.mysharedScoresheets = async (req, res) => {
  const { userid, ScoresheetId } = req.body;
  if (wantToView === "yes")
    res.sendStatus(200).redirect("/scoreSheet/viewScoresheet:ScoresheetId");
  else {
    //Here the user will be deleting the Scoresheet.
    //Redirecting to the delete route of scoreSheet.
    res.sendStatus(200).redirect("/scoreSheet/deleteScoresheet:ScoresheetId");
  }
};
