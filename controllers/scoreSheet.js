// const { findById } = require("../models/Scoresheet");
const Scoresheet = require("../models/Scoresheet");

exports.createScoresheet = async (req, res) => {
  const result = await new Scoresheet(req.body).save();
  res.json({ result, message: "Score Sheet created successfully." });
};

exports.viewScoresheet = async (req, res) => {
  // return console.log(typeof req.body);
  const result = await Scoresheet.findById(req.body.id);
  res.json({
    result,
  });
};

exports.deleteScoresheet = async (req, res) => {
  const result = await Scoresheet.findByIdAndDelete(req.params.id);
  // return console.log(result);
  res.json({ result, message: "Scoresheet deleted successfully." });
};
