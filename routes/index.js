const router = require("express").Router();

router.use("/user", require("./user"));
router.use("/scoreSheet", require("./scoreSheet"));

module.exports = router;
