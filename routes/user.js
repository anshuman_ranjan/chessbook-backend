const router = require("express").Router();
const usercontroller = require("../controllers/user");

router.post("/registerUser", usercontroller.registerUser);
router.get("/shareScoresheet", usercontroller.shareScoresheet);
router.get("/mysharedScoresheets", usercontroller.mysharedScoresheets);
router.get(
  "/viewScoresheetSharedWithMe",
  usercontroller.viewScoresheetSharedWithMe
);

module.exports = router;
