const router = require("express").Router();
const scoreSheetcontroller = require("../controllers/scoreSheet");

router.post("/createScoresheet", scoreSheetcontroller.createScoresheet);
router.get("/viewScoresheet", scoreSheetcontroller.viewScoresheet);
router.delete("/deleteScoresheet", scoreSheetcontroller.deleteScoresheet);

module.exports = router;
