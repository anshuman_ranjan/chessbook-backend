const express = require("express");
const app = express();
const router = require("./routes/index");
const port = 3000;
require("./connection/connect");

app.use(express.json());
app.use("/", router);

app.listen(port, (err) => {
  if (err) console.log(`Error: `, err);
  else console.log(`Server listening on port,`, port);
});
