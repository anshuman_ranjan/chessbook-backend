const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//User Schema
const UserSchema = Schema(
  {
    email: {
      type: String,
      default: "",
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("User", UserSchema);
