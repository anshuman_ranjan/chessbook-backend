const mongoose = require("mongoose");
const User = require("./User");
const Schema = mongoose.Schema;
const Model = mongoose.model;
const ObjectId = mongoose.Schema.Types.ObjectId;

//User Schema
const ScoresheetSchema = Schema(
  {
    name: { type: String, default: "" },
    sharedBy: { type: ObjectId, ref: "User", default: null },
    sharedWith: [
      {
        user: { type: ObjectId, ref: "User", default: null },
        isAccepted: { type: Boolean, default: false },
      },
    ],
    sheetData: { type: String, default: "" },
    isDeleted: { type: Boolean, default: false },
  },
  { timestamps: true }
);

const Scoresheet = Model("Scoresheet", ScoresheetSchema);

module.exports = Scoresheet;
